/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eitan;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 *
 * @author eitan
 */
public class SaxXMLHelper {
    public static XMLStreamWriter open(String filename) {
        try {
            OutputStream outputStream = new FileOutputStream(new File(filename));
            XMLStreamWriter out = XMLOutputFactory.newInstance().createXMLStreamWriter(
                    new OutputStreamWriter(outputStream, "utf-8"));
            out.writeStartDocument();
            return out;
        } catch (UnsupportedEncodingException | XMLStreamException | FileNotFoundException ex) {
            Logger.getLogger(SaxXMLHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static XMLStreamWriter open(ByteArrayOutputStream bos) {
        try {
            OutputStream outputStream = bos;
           
            XMLStreamWriter out = XMLOutputFactory.newInstance().createXMLStreamWriter(
                    new OutputStreamWriter(outputStream, "utf-8"));
            out.writeStartDocument();
            return out;
        } catch (UnsupportedEncodingException | XMLStreamException ex) {
            Logger.getLogger(SaxXMLHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void close(XMLStreamWriter out) {
        try {
            out.writeEndDocument();
            out.close();
        } catch (XMLStreamException ex) {
            Logger.getLogger(SaxXMLHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

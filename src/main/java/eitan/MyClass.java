package eitan;

import static eitan.RandomItem.sdf;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.annotate.JsonAutoDetect;
public class MyClass {
    public static final int NUM_ALL = 10000000;
    public static final int NUM_OF_A_NODES = 200000;
    public static final String FILENAME = "db.json";
    
    public static void main(String[] args) throws XMLStreamException, IOException {
        JsonFactory jsonFactory = new JsonFactory(); // or, for data binding, org.codehaus.jackson.mapper.MappingJsonFactory
        JsonGenerator jg = jsonFactory.createJsonGenerator(new File(FILENAME), JsonEncoding.UTF8);
        System.out.println("writing random db to: "+FILENAME);
        Random rand = new Random();
        List<Node> list = new ArrayList<>(NUM_OF_A_NODES);
        for (int i = 0; i < NUM_OF_A_NODES; i++) {
            Node node = new Node(i);
            int dataNum = getGausian(rand, 3, 1);
            int relationNum = getGausian(rand, 200, 245);
            for (int j = 0; j < dataNum; j++)
                node.data.add(new RandomItem(j, rand));
            for (int j = 0; j < relationNum; j++)
                node.links.add(rand.nextInt(NUM_ALL));
            node.writeJson(jg);
            if (i % 1000 == 0)
                System.out.println(""+100*i/NUM_OF_A_NODES+"% completed");
        }
        jg.close();
    }
    
    private static int getGausian(Random rand, double std, double mean) {
        return (int) Math.round(Math.max(rand.nextGaussian() * std + mean, 0));
    }
}

//@JsonAutoDetect
class RandomItem {
    public static Date cd = new Date();
    public static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
    public RandomItem(int id, Random rand) {
        this.id = id;
        name = UUID.randomUUID().toString();
        date = new Date(cd.getTime() - (long) rand.nextInt(3 * 365) * 24l * 3600l * 1000l);
        category = Integer.toHexString(rand.nextInt(400));
    }
    final int id;
    final String name;
    final Date date;
    final String category;
    
    public int getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    public String getDate() {
        return sdf.format(date);
    }
    
    public String getCategory() {
        return category;
    }
    
    @Override
    public String toString() {
        return "Item{" + "name=" + name + ", date=" + date + ", category=" + category + '}';
    }
    
    void writeJson(JsonGenerator out) throws IOException  {
        out.writeStartObject();
        out.writeFieldName("id");
        out.writeNumber(id);
        out.writeFieldName("category");
        out.writeString(category);
        out.writeFieldName("date");
        out.writeString(sdf.format(date));
        out.writeEndObject();
    }
};

@JsonAutoDetect
class Node {
    final int id;
    final List<Integer> links;
    final List<RandomItem> data;
    
    public Node(int id) {
        this.id = id;
        links = new ArrayList<>();
        data = new ArrayList<>();
    }
    
    @Override
    public String toString() {
        return "Node{" + "links=" + links + ", data=" + data + '}';
    }
    
    void writeJson(JsonGenerator out) throws IOException  {
        out.writeStartObject();
        
        out.writeFieldName("id");
        out.writeNumber(id);
        
        out.writeFieldName("data");
        out.writeStartArray();
        for (RandomItem randomItem : data) {
            randomItem.writeJson(out);
        }
        out.writeEndArray();
        
        out.writeFieldName("links");
        out.writeStartArray();
        for (Integer integer : links) {
            out.writeNumber(integer);
        }
        out.writeEndArray();
        
        out.writeEndObject();
    }
}
